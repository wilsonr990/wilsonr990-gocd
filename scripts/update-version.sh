#!/bin/bash
set -eux

if [ -z ${1+x} ]; then
  echo "ERROR: one parameter required: repository"
  exit 1
fi

repository=$1 # git@gitlab.com:rgapps/infrastructure.git

if [ -d "repo" ]; then
  rm -rf repo
fi

git clone --depth 1 --branch master "${repository}" repo
cd repo || exit 1
curl -s "https://gitlab.com/rgapps/infrastructure/raw/dev/scripts/prepare-new-version.sh" | bash

git add version.txt

echo "CICD: Release version $(head -1 version.txt)" | git commit -F -
git push
cd ..
