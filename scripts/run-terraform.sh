#!/bin/bash
# only x on purpose. Changes in terraform files should be updated no matter if they failed
set -x

# check requirements
if [[ $(< inputs/encrypted) != "false" ]]; then
  echo "ERROR: inputs seems to be encrypted!"
  exit 1
fi
if [ ! -f "inputs/terraform/variables.tf" ]; then
  echo "ERROR: inputs/terraform/variables.tf file not  found"
  exit 1
fi
if [ ! -d "terraform" ]; then
  echo "ERROR: terraform folder not  found"
  exit 1
fi

if [ $# -eq 0 ] ||  { [ "$1" != "true" ] && [ "$1" != "false" ] && [ "$1" != "destroy" ] ]; }; then
  echo "ERROR: need a parameter. (true to deploy false to undeploy)"
  exit 1
fi

# set deploy_dev var
if [ "$1" != "destroy" ]; then
  sed -i '' "s/^.*\/\/ deploy-dev$/  default = $1 \/\/ deploy-dev/" inputs/terraform/variables.tf
fi
if [ $# -ge 2 ] && [ -n "$2" ]; then
  echo "INFO: setting IP"
  sed -i '' "s/^.*\/\/ remote-public-ip$/  default = \"$2\" \/\/ remote-public-ip/" inputs/terraform/variables.tf
fi

# make sure last changes are pulled
git pull

# restore current state!
mv inputs/terraform/terraform.tfstate terraform/terraform.tfstate
mv inputs/terraform/.terraform.lock.hcl terraform/.terraform.lock.hcl

# replace variables
mv terraform/variables.tf terraform/variables.tmp
cp inputs/terraform/variables.tf terraform/variables.tf

# replace
cp -rf inputs/terraform/aws_key.pub ~/.ssh/aws_key.pub
cp -rf inputs/terraform/aws_key.pem ~/.ssh/aws_key.pem
chmod 500 ~/.ssh/aws_key.pub
chmod 500 ~/.ssh/aws_key.pem

# run terraform
cd terraform || exit 1

# initialize if needed
if [ ! -d ".terraform" ]; then
  terraform init
fi

if [ "$1" == "destroy" ]; then
  terraform destroy --auto-approve
else
  terraform apply --auto-approve
fi

cd ..

# restore variables
rm terraform/variables.tf
mv terraform/variables.tmp terraform/variables.tf

# save current state!
mv terraform/terraform.tfstate inputs/terraform/terraform.tfstate
mv terraform/.terraform.lock.hcl inputs/terraform/.terraform.lock.hcl
rm terraform/terraform.tfstate.backup

# commit state change!
if [ "$(git log -1 --pretty=%B | sed -n 1p)" == "update terraform state!" ]; then
  git reset --soft HEAD~1
fi

git restore --staged .
git add inputs/**
git commit -m "update terraform state!"
