#!/bin/bash

version=$(head -1 version.txt)
major=0
minor=0
build=0

# commit state change!
if [ "$(git log -1 --pretty=%B | sed -n 1p)" == "CICD: Release version $version" ]; then
  echo "Keep old version (Repeated Release)"
  exit 1
fi

# break down the version number into it's components
regex="([0-9]+).([0-9]+).([0-9]+)"
changes=20
if [[ $version =~ $regex ]]; then
  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  build="${BASH_REMATCH[3]}"

  # identify number of changes
  changes=$(git rev-list --count HEAD ^"${version}")
fi

echo "number of changes: ${changes}"

if [[ $changes == 0 ]]; then
  echo "Keep old version (No changes)"
  exit 1
fi

# increase version
if [[ $changes -lt 4 ]]; then
  build=$((build + 1))
elif [[ $changes -gt 3 && $changes -lt 100 && $minor -lt 20 ]]; then
  minor=$((minor + 1))
  build=0
else
  major=$((major + 1))
  minor=0
  build=0
fi

# echo the new version number
echo "new version: ${major}.${minor}.${build}"
echo "${major}.${minor}.${build}" >version.txt
