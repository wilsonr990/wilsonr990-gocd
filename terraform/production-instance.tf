resource "aws_instance" "production-instance" {
  count           = 1
  ami             = var.aws-ami-id
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.aws-key.0.id
  security_groups = [
    aws_security_group.ssh.0.name,
    aws_security_group.internet-access.0.name,
    aws_security_group.production-page.0.name
  ]
  root_block_device {
    volume_size = 30
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"Instance is up and running.\""
    ]

    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "ec2-user"
      private_key = file(var.ssh-key-private)
      timeout     = "2m"
    }
  }

  tags = {
    Name = "production-instance"
  }
}

resource "null_resource" "production-instance-provisioner" {
  count    = 1
  triggers = {
    this_file           = sha1(file("${path.cwd}/production-instance.tf"))
    playbook            = sha1(file("${path.cwd}/../ansible-playbooks/install-docker.yml"))
    production_instance = aws_instance.production-instance.0.id
  }

  depends_on = [
    aws_instance.production-instance
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.production-instance.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "../ansible-playbooks/install-docker.yml"
    ])
  }
}

resource "null_resource" "production-instance-configuration" {
  # TODO: commented due to go-daddy not supporting API any more. Changing provider to cloudflare
  count    = 0
  triggers = {
    this_file           = sha1(file("${path.cwd}/production-instance.tf"))
    playbook            = sha1(file("${path.cwd}/../ansible-playbooks/configure-go-daddy.yml"))
    production-instance = aws_instance.production-instance.0.public_ip
  }
  depends_on = [
    null_resource.production-instance-execution
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.production-instance.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'GODADDY_KEY=${var.godaddy-key}",
      "  GODADDY_SECRET=${var.godaddy-secret}",
      "  PRODUCTION_IP=${aws_instance.production-instance.0.public_ip}'",
      "../ansible-playbooks/configure-go-daddy.yml"
    ])
  }
}

variable "remote-shared-path" {
  type    = string
  default = "/"
}

output "production-instance-ssh" {
  value = length(aws_instance.production-instance) != 0? "ssh -i '${var.ssh-key-private}' ec2-user@${aws_instance.production-instance.0.public_ip}" : ""
}
