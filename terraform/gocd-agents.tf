resource "null_resource" "dev-gocd-agent-execution" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file                 = sha1(file("${path.cwd}/gocd-agents.tf"))
    playbook                  = sha1(file("${path.cwd}/../ansible-playbooks/install-dev-gocd-agent.yml"))
    volumes                   = sha1(join("", [for f in fileset("${path.cwd}/../inputs/multipurpose-agent", "*") : filesha1("${path.cwd}/../inputs/multipurpose-agent/${f}")]))
    dockerfile                = sha1(file("${path.cwd}/../docker-images/multipurpose-agent/Dockerfile"))
    gocd_server_configuration = null_resource.gocd-server-configuration.0.id
    gocd_server_ip            = aws_instance.gocd-server.0.public_ip
  }

  // null_resource.gocd-server-configuration instead of aws_instance.gocd-server due to playboks/output content dependency
  depends_on = [
    null_resource.gocd-server-configuration
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${var.local-ssh-private-ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'SHARED_PATH=${var.local-shared-path}",
      "  GOCD_SERVER_IP=${aws_instance.gocd-server.0.public_ip}",
      "  QA_PUBLIC_IP=${var.local-private-ip}",
      "  GOCD_SERVER_PUBLIC_IP=${aws_instance.gocd-server.0.public_ip}",
      "  INSTANCE=${count.index}'",
      "../ansible-playbooks/install-dev-gocd-agent.yml"
    ])
  }
}

resource "null_resource" "qa-gocd-agent-execution" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file                 = sha1(file("${path.cwd}/gocd-agents.tf"))
    playbook                  = sha1(file("${path.cwd}/../ansible-playbooks/install-qa-gocd-agent.yml"))
    volumes                   = sha1(join("", [
      for f in fileset("${path.cwd}/../inputs/nginx", "**") :filesha1("${path.cwd}/../inputs/nginx/${f}")
    ]))
    dockerfile                = sha1(file("${path.cwd}/../docker-images/docker-agent/Dockerfile"))
    gocd_server_configuration = null_resource.gocd-server-configuration.0.id
    gocd_server_ip            = aws_instance.gocd-server.0.public_ip
  }

  // null_resource.gocd-server-configuration instead of aws_instance.gocd-server due to playboks/output content dependency
  depends_on = [
    null_resource.gocd-server-configuration
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${var.local-ssh-private-ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'SHARED_PATH=${var.local-shared-path}",
      "    GOCD_SERVER_IP=${aws_instance.gocd-server.0.public_ip}'",
      "../ansible-playbooks/install-qa-gocd-agent.yml"
    ])
  }
}

resource "null_resource" "production-instance-execution" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file                 = sha1(file("${path.cwd}/gocd-agents.tf"))
    playbook                  = sha1(file("${path.cwd}/../ansible-playbooks/install-production-agent.yml"))
    volumes                   = sha1(join("", [
      for f in fileset("${path.cwd}/../inputs/nginx", "**") :filesha1("${path.cwd}/../inputs/nginx/${f}")
    ]))
    dockerfile                = sha1(file("${path.cwd}/../docker-images/docker-agent/Dockerfile"))
    production-instance       = aws_instance.production-instance.0.public_ip
    gocd_server_configuration = null_resource.gocd-server-configuration.0.id
    gocd_server_ip            = aws_instance.gocd-server.0.public_ip
  }

  // null_resource.gocd-server-configuration instead of aws_instance.gocd-server due to playboks/output content dependency
  depends_on = [
    null_resource.gocd-server-configuration,
    null_resource.qa-gocd-agent-execution
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.production-instance.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'SHARED_PATH=${var.remote-shared-path}",
      "    GOCD_SERVER_IP=${aws_instance.gocd-server.0.public_ip}'",
      "../ansible-playbooks/install-production-agent.yml"
    ])
  }
}
